import argparse
import requests
from requests_toolbelt.adapters import host_header_ssl
from urllib.parse import quote as encodeURI
import os

ap = argparse.ArgumentParser()
ap.add_argument("tag", help="Pixiv tag(s) to search")
ap.add_argument("-p", dest="numpages", type=int, default=1, help="number of pages to download (default 1)")
ap.add_argument("-s", dest="startpagenum", type=int, default=1, help="page number to start at")
ap.add_argument("-r", action='store_true', help="include r18 posts")
args = ap.parse_args()

PHPSESSID = None
with open("PHPSESSID.txt", 'r') as f:
	PHPSESSID = f.read()

rqs = requests.Session()
rqs.mount('https://', host_header_ssl.HostHeaderSSLAdapter())

download_count = 1
for i in range(args.startpagenum, args.numpages+1):
	page_url = f"https://210.140.131.219/ajax/search/artworks/{encodeURI(args.tag, safe='')}?order=popular_d&mode={'all' if args.r else 'safe'}&p={i}"
	print("get", page_url)
	page_data = rqs.get(page_url, cookies={"PHPSESSID": PHPSESSID}, headers={"host":"www.pixiv.net"}).json()
	if (page_data['error']):
		print(page_data['message'])
		exit(1)
	for illust in page_data['body']['illustManga']['data']:
		illust_r18 = bool(illust['xRestrict'])
		illust_url = f"https://210.140.131.219/ajax/illust/{illust['id']}/pages"
		print("get", illust_url)
		illust_data = rqs.get(illust_url, headers={"host":"www.pixiv.net"}).json()
		if (illust_data['error']):
			print(illust_data['message'])
		else:
			for image in illust_data['body']:
				image_url = image['urls']['original']
				download_dir = f"download/{args.tag}/"
				os.makedirs(download_dir, exist_ok=True)
				download_filename = str(download_count) + '_' + ('x_' if illust_r18 else '') + image_url.split('/').pop()
				download_path = download_dir + download_filename
				if os.path.exists(download_path):
					print(download_path, "already exists")
					continue
				print("get", image_url)
				res = rqs.get(image_url, headers={'referer':'https://www.pixiv.net'})
				with open(download_path, "wb") as f:
					f.write(res.content)
				print("saved", download_filename)
				download_count = download_count + 1
