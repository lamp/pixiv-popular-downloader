This is a python script for downloading original pixiv images from popular search results via a premium account.

# Instructions

1. Download this repo to your computer of course, and open the terminal in it. Run `pip install -r requirements.txt` if necessary.

2. In your browser, on Pixiv logged in to a premium account, in dev tools Application tab, copy the **value** of the `PHPSESSID` cookie, and paste it into a new file named `PHPSESSID.txt` in this folder.

3. Run `python pixiv-popular-downloader.py -h` for usage information. Example usage to download 10 pages of 初音ミク tag, including r18: `python pixiv-popular-downloader.py -r -p 10 "初音ミク"`

4. Check the download folder. If you're getting newest results instead of popular results, then your PHPSESSID failed to work.
